/**=========================================================
 * Module: ConstructorController
 =========================================================*/

App.controller('ConstructorController', ['$scope', '$state', '$rootScope',
    function ($scope, $state, $rootScope) {
        'use strict';
        var self = this;
        var pageName = '';

        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                self.page = toState.url.split('/')[2];
                self.form = self.page === 'pagecreator';

                var d = new Date();
                var n = d.getTime();

                $.get('app/views/templates/' + self.page + '.html?var=' + n)
                    .then(function (template) {
                        $('#contentarea').html(template);
                    });

            });

        self.page = $state.current.url.split('/')[2];
        self.form = self.page === 'pagecreator';

        var d = new Date();
        var n = d.getTime();

        $.get('app/views/templates/' + self.page + '.html?var=' + n)
            .then(function (template) {

                contentArea.html(template);
                if (self.page !== 'pagecreator') {
                    contentArea.contentbuilder({
                        zoom: 1,
                        snippetOpen: true,
                        snippetFile: 'assets/minimalist-basic/snippets.html',
                        toolbar: 'left'
                    });
                    contentArea.css({zoom: 1});
                }
            });


        function saveNewPage(result) {
            var d = new Date();
            var n = d.getTime();

            $.get('server/sidebar/sidebar-items'+'.json?var=' + n)
                .then(function (sidebar) {
                    console.log('sidebar')
                    console.log(sidebar)
                    var sidebarJson = sidebar;
                    if (!(sidebarJson instanceof Object)) {
                        sidebarJson = JSON.parse(sidebar);
                    }

                    var pos = sidebarJson.map(function (e) {
                        return e.text;
                    }).indexOf('CONTENTBUILDER');

                    var newPage = {
                        sref: "app.contentbuilder." + pageName,
                        text: pageName.charAt(0).toUpperCase() + pageName.slice(1),
                        translate: "sidebar.nav.pages." + pageName.toUpperCase()
                    };
                    sidebarJson[pos].subnav.push(newPage);
                    saveApiCall('server/sidebar/sidebar-items', sidebarJson, false, 'json');
                });

            $.get('app/langs/en'+ '.json?var=' + n)
                .then(function (en) {
                    console.log('en')
                    console.log(en)
                    var englishJson = en;
                    if (!(englishJson instanceof Object)) {
                        englishJson = JSON.parse(en);
                    }
                    englishJson.sidebar.nav.pages[pageName.toUpperCase()] = pageName.charAt(0).toUpperCase() + pageName.slice(1);
                    saveApiCall('app/langs/en', englishJson, false, 'json');
                });

            $.get('app/js/app'+ '.js?var=' + n)
                .then(function (config) {

                    var newState = ".state('app.contentbuilder." + pageName + "', {"
                        + "\t" + "url: '/contentbuilder/" + pageName + "',"
                        + "\t" + "templateUrl: basepath('templates/" + pageName + ".html')\n"
                        + "\t})";

                    var newApp = config.replace("// Contentbuilder-block ", newState + "\n" + "// Contentbuilder-block ");
                    console.log('newApp')
                    console.log(newApp)

                    saveApiCall('app/js/app', newApp, false, 'js');
                });
            makeCompleteHtml(result, pageName);
            saveApiCall(pageName, result, true, 'html');
        }

        self.add = function (newPage) {
            self.form = false;
            pageName = newPage;

            contentArea.contentbuilder({
                zoom: 1,
                snippetOpen: true,
                snippetFile: 'assets/minimalist-basic/snippets.html',
                toolbar: 'left'
            });
            contentArea.css({zoom: 1});


        };


        var bucket = 'pronkeadmin.studioio.nl';

        function saveApiCall(fileName, body, template, format) {
            if (template) {
                var path = 'app/views/templates/' + fileName + '.html';
            } else {
                var path = fileName + '.' + format;
            }

            gapi.client.load('storage', 'v1', function () {
                var request = gapi.client.request({
                    path: '/upload/storage/v1/b/' + bucket + '/o',
                    method: 'POST',
                    params: {
                        uploadType: 'media',
                        name: path
                    },
                    headers: {
                        'Content-Type': 'text/html'
                    },
                    body: body
                });
                request.execute(function (resp) {
                    console.log(resp);
                });
            });
        }

        self.saveHtml = function () {

            $('#contentarea').data('contentbuilder').viewHtml();

            $('#md-html').css({display: 'none'});
            var result = $('#txtHtml').val();
            if (self.page === 'pagecreator') {
                saveNewPage(result);
            } else {
                makeCompleteHtml(result, self.page);
                saveApiCall(self.page, result, true, 'html');
            }
        };

        function makeCompleteHtml(body, fileName) {
            var d = new Date();
            var n = d.getTime();
            var result = '';
            $.get('app/views/templates/header' + '.html?var=' + n)
                .then(function (header) {
                    result += header;
                    $.get('app/views/templates/footer' + '.html?var=' + n)
                        .then(function (header) {
                            result = result + body + header;
                            saveApiCall(fileName, result, false, 'html');
                        })
                })
        }

        self.viewHtml = function () {
            $('#contentarea').data('contentbuilder').viewHtml();
        };
    }])
;
